package org.green.jwt.demo.jpa.repository.transaction;

import org.green.jwt.demo.jpa.entity.transaction.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, UUID> {

    List<UserEntity> findByUserName(String username);

    @Modifying
    @Query("UPDATE UserEntity u SET u.deleted = true WHERE u.userId = :userId")
    int markUserAsDeleted(@Param("userId") UUID userId);
}
