package org.green.jwt.demo.security.provider;

import org.green.jwt.demo.jpa.entity.transaction.UserEntity;
import org.green.jwt.demo.jpa.entity.transaction.UserRole;
import org.green.jwt.demo.jpa.repository.transaction.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class BasicAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Transactional
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = authentication.getName();
        String password = authentication.getCredentials().toString();

        List<UserEntity> userEntities = userRepository.findByUserName(username);
        if (userEntities.isEmpty()) {
            throw new UsernameNotFoundException("Can't find user with username: " + username);
        }

        UserEntity userEntity = userEntities.getFirst();

        if (!passwordEncoder.matches(password, userEntity.getPwd())) {
            throw new BadCredentialsException("Invalid password!");
        }

        List<UserRole> userRoles = userEntity.getUserRoles();

        List<GrantedAuthority> authorities = userRoles.stream()
                .map(UserRole::getRefRole)
                .map(refRole -> new SimpleGrantedAuthority(refRole.getRoleName()))
                .collect(Collectors.toList());

        return new UsernamePasswordAuthenticationToken(username, password, authorities);
    }
    @Override
    public boolean supports(Class<?> authentication) {
        return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
    }
}
