package org.green.jwt.demo.rest.error;

import org.springframework.http.HttpStatus;

public enum ErrorCode {
    USER_NOT_FOUND(HttpStatus.NOT_FOUND, "This user does not exist"),
    USER_IS_LOCKED(HttpStatus.UNPROCESSABLE_ENTITY, "This user is locked");

    private HttpStatus httpStatus;
    private String message;

    private ErrorCode(HttpStatus httpStatus, String message) {
        this.httpStatus = httpStatus;
        this.message = message;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public String getMessage() {
        return message;
    }
}
