package org.green.jwt.demo.rest.controller;

import org.green.jwt.demo.jpa.exception.DataNotFoundException;
import org.green.jwt.demo.rest.error.ErrorCode;
import org.green.jwt.demo.rest.model.UserModel;
import org.green.jwt.demo.service.AdminService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ProblemDetail;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/admin/v1")
public class AdminController {
    private static final Logger LOG = LoggerFactory.getLogger(AdminController.class);

    @Autowired
    private AdminService adminService;

    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers() {
        return ResponseEntity.ok(adminService.getUsers());
    }

    @GetMapping("/user/{userId}")
    public ResponseEntity<UserModel> getUser(@PathVariable UUID userId) {
        LOG.info("/user");
        UserModel responseUserModel;
        try {
            responseUserModel = adminService.getUser(userId);
        } catch (DataNotFoundException dnf) {
            LOG.error(dnf.getMessage());
            return ResponseEntity.of(ProblemDetail.forStatusAndDetail(ErrorCode.USER_NOT_FOUND.getHttpStatus(),
                            "User with given Id not found: " + userId))
                    .build();
        }
        return ResponseEntity.ok(responseUserModel);
    }

    @PostMapping("/register/user")
    public ResponseEntity<UserModel> registerUser(@RequestBody UserModel userModel) {
        return ResponseEntity.ok(adminService.register(userModel));
    }

    @PostMapping("/update/user")
    public ResponseEntity<UserModel> updateUser(@RequestBody UserModel userModel) {
        UserModel responseUserModel;
        try {
            responseUserModel = adminService.updateUser(userModel);
        } catch (DataNotFoundException dnf) {
            LOG.error(dnf.getMessage());
            return ResponseEntity.of(ProblemDetail.forStatusAndDetail(ErrorCode.USER_NOT_FOUND.getHttpStatus(),
                            "User with given Id not found: " + userModel.getUserId()))
                    .build();
        }
        return ResponseEntity.ok(responseUserModel);
    }

    @DeleteMapping("/user/{userId}")
    public ResponseEntity<Boolean> deleteUser(@PathVariable UUID userId) {
        Boolean isDeleted = false;
        try {
            isDeleted = adminService.deleteUser(userId);
        } catch (DataNotFoundException dnf) {
            LOG.error(dnf.getMessage());
            return ResponseEntity.of(ProblemDetail.forStatusAndDetail(ErrorCode.USER_NOT_FOUND.getHttpStatus(),
                            "User with given Id not found: " + userId))
                    .build();
        }

        return ResponseEntity.ok(isDeleted);
    }

    @GetMapping("/user/role")
    public ResponseEntity<List<String>> getRole(Authentication authentication) {
        List<String> authorities = authentication.getAuthorities()
                .stream()
                .map(grantedAuthority -> {
                    return ((SimpleGrantedAuthority) grantedAuthority).getAuthority();
                })
                .collect(Collectors.toList());

        return ResponseEntity.ok(authorities);
    }
}
