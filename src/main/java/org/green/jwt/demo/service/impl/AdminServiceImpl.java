package org.green.jwt.demo.service.impl;

import org.green.jwt.demo.jpa.entity.reference.RefRole;
import org.green.jwt.demo.jpa.entity.transaction.UserEntity;
import org.green.jwt.demo.jpa.entity.transaction.UserRole;
import org.green.jwt.demo.jpa.exception.DataNotFoundException;
import org.green.jwt.demo.jpa.repository.reference.RefRoleRepository;
import org.green.jwt.demo.jpa.repository.transaction.UserRepository;
import org.green.jwt.demo.jpa.repository.transaction.UserRoleRepository;
import org.green.jwt.demo.rest.model.UserModel;
import org.green.jwt.demo.service.AdminService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class AdminServiceImpl implements AdminService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private RefRoleRepository refRoleRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public UserModel register(UserModel user) {
        UserEntity userEntity = new UserEntity();
        userEntity.setUserId(UUID.randomUUID());
        userEntity.setMobileNo(user.getMobileNumber());
        userEntity.setEmail(user.getEmail());
        userEntity.setUserName(user.getUsername());
        userEntity.setPwd(passwordEncoder.encode(user.getPwd()));
        userEntity.setFullname(user.getFullName());
        UserEntity newUserEntity = userRepository.save(userEntity);

        List<UserRole> userRoles = new ArrayList<>();
        for (String role : user.getRoles()){
            RefRole refRole = refRoleRepository.findByRoleName(role);

            if (refRole != null) {
                UserRole userRole = new UserRole();
                userRole.setUserRoleId(UUID.randomUUID());
                userRole.setUser(newUserEntity);
                userRole.setRefRole(refRole);
                userRoles.add(userRole);
            }
        }

        List<UserRole> userRoleList = (List<UserRole>) userRoleRepository.saveAll(userRoles);
        List<String> roleNames = userRoleList.stream().map(UserRole::getRefRole).map(RefRole::getRoleName).collect(Collectors.toList());

        user.setRoles(roleNames);
        return user;
    }

    @Override
    public boolean deleteUser(UUID userId) throws DataNotFoundException {
        int rowsAffected = userRepository.markUserAsDeleted(userId);

        if (rowsAffected == 0) {
            // No user found with the given userId
            throw new DataNotFoundException("User with id " + userId + " not found");
        }
        return true;
    }

    @Override
    public UserModel updateUser(UserModel user) throws DataNotFoundException {
        return null;
    }

    @Override
    public UserModel getUser(UUID userId) throws DataNotFoundException {
        UserEntity userEntity = fetchUser(userId);
        List<UserRole> userRoles = userEntity.getUserRoles();

        List<String> roles = userRoles.stream().map(UserRole::getRefRole).map(RefRole::getRoleName).collect(Collectors.toList());

        UserModel userModel = new UserModel();
        userModel.setUserId(userEntity.getUserId());
        userModel.setUsername(userEntity.getUserName());
        userModel.setFullName(userEntity.getFullname());
        userModel.setEmail(userEntity.getEmail());
        userModel.setRoles(roles);

        return userModel;
    }

    @Override
    public List<UserModel> getUsers() {
        List<UserEntity> userEntities = userRepository.findAll();

        return userEntities.stream()
                .map(userEntity -> modelMapper.map(userEntity, UserModel.class))
                .collect(Collectors.toList());
    }

    private UserEntity fetchUser(UUID userId) throws DataNotFoundException {
        Optional<UserEntity> userEntityOptional =  userRepository.findById(userId);

        if (userEntityOptional.isEmpty()) {
            throw new DataNotFoundException("Data not found for userId: " + userId);
        }

        return userEntityOptional.get();
    }
}
