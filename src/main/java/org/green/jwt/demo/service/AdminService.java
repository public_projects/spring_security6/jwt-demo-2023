package org.green.jwt.demo.service;

import org.green.jwt.demo.jpa.exception.DataNotFoundException;
import org.green.jwt.demo.rest.model.UserModel;

import java.util.List;
import java.util.UUID;

public interface AdminService {
    UserModel register(UserModel user);
    boolean deleteUser(UUID userId) throws DataNotFoundException;
    UserModel updateUser(UserModel user) throws DataNotFoundException;
    UserModel getUser(UUID userId) throws DataNotFoundException;
    List<UserModel> getUsers();
}
