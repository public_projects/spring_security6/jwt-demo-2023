package org.green.jwt.demo.config;

import org.green.jwt.demo.security.filter.CsrfCookieFilter;
import org.green.jwt.demo.security.filter.JWTTokenGeneratorFilter;
import org.green.jwt.demo.security.filter.JWTTokenValidatorFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.csrf.CsrfTokenRequestAttributeHandler;

@Configuration
public class SecurityConfiguration {

    @Bean
    public SecurityFilterChain defaultSecurityFilterChain(HttpSecurity http) throws Exception {
        // Temporary fix, without this will get "status": 403,"error": "Forbidden",
        http.csrf(httpSecurityCsrfConfigurer -> {httpSecurityCsrfConfigurer.disable();});

        http.sessionManagement(httpSecuritySessionManagementConfigurer -> {
            httpSecuritySessionManagementConfigurer.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        });

        http.authorizeHttpRequests(requests -> {
            // Always allow to register without any authentication
            requests.requestMatchers("/admin/v1/register/user").permitAll();
            requests.requestMatchers("/user/role").authenticated();
            requests.requestMatchers(HttpMethod.GET, "/admin/v1/user/**", "/admin/v1/users").hasRole("ADMIN");
            requests.requestMatchers(HttpMethod.DELETE, "/admin/v1/user/**").hasRole("ADMIN");
        });

        http.addFilterAfter(new CsrfCookieFilter(), BasicAuthenticationFilter.class);
        http.addFilterAfter(new JWTTokenGeneratorFilter(), BasicAuthenticationFilter.class);
        http.addFilterBefore(new JWTTokenValidatorFilter(), BasicAuthenticationFilter.class);

        http.httpBasic(Customizer.withDefaults());
        return http.build();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
