package org.green.jwt.demo.config;

public interface SecurityConstants {

    /**
     * This is the secret key value
     */
    String JWT_KEY = "jxgEQeXHuPq8VdbyYFNkANdudQ53YUn4";
    String JWT_HEADER = "Authorization";

}

