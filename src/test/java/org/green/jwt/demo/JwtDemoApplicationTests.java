package org.green.jwt.demo;

import org.green.jwt.demo.jpa.entity.reference.RefRole;
import org.green.jwt.demo.jpa.repository.reference.RefRoleRepository;
import org.green.jwt.demo.jpa.repository.transaction.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class JwtDemoApplicationTests {

    @Autowired
    private RefRoleRepository refRoleRepository;

    @Autowired
    private UserRepository userRepository;

    @Test
    void hasBaseRefRole() {
        Iterable<RefRole> refRoles = refRoleRepository.findAll();
        Assertions.assertTrue(refRoles.iterator().hasNext());
    }

    @Test
    void hasUser() {
        long count = userRepository.count();
        Assertions.assertTrue(count > 0);
    }

}
